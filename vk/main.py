import vk
import requests
from time import sleep

session = vk.Session(access_token='faf2c1fe89ac51cd74fc67f5b79e80aba247b34679d87d459cb07e91215ea80f98f6e7fb360eb3ac5c48f')

api = vk.API(session=session)

start = 0
count = 200

for offset in range(start, 2000, count):
    attachments = api.messages.getHistoryAttachments(peer_id='34421354', media_type='photo', start_from=offset, count=count)
    for key, value in attachments.items():
        url_small = value['photo']['src_small']
        with open('photos/' + str(value['photo']['pid']) + '.jpg', 'wb') as handle:
            response = requests.get(url_small, stream=True)
            for block in response.iter_content(1024):
                if not block:
                    break
            handle.write(block)
    sleep(2)
    print('repeat')

