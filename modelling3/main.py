
import pylab
from matplotlib.mlab import frange


def euler(f, xa, xb, ya, h):
    n = int((xb - xa) / h)
    x = q_const
    y = ya
    ylist = []
    xlist = []
    for i in range(n):
        y += h * f(x, y)
        ylist.append(y)
        x += h
        xlist.append(x)
    return ylist, xlist


q1s = 2900

ls = 3300
lk = 4200
dl = 450

rs = 2300
rk = 2900
dr = 300

cs = 1800
ck = 2700
dc = 450

t0 = 0
tk = 5
dt = 0.0005

q_const = 4
e = 1900

pylab.figure()
pylab.xlabel('x label')
pylab.ylabel('y label')
clist, llist, rlist = frange(cs, ck, dc).tolist(), frange(ls, lk, dl).tolist(), frange(rs, rk, dr).tolist()


def calc(l, r, c):

    ylist, xlist = euler(lambda x, y: ((e - r * y - x * (1.0/c)) / l), t0, tk, q1s, dt)

    # print(ylist, xlist)
    pylab.plot(xlist, ylist, label='L={0}, R={1}, C={2}'.format(l, r, c))

for k, m, c in zip(clist, llist, rlist):
    calc(k, m, c)

pylab.legend()
pylab.show()

