import socket
import auth, common

sock = socket.socket()

sock.connect(('localhost', 9009))

auth.dh_client(sock)

message = common.prepare("hello")

integrity = common.send_block(sock, message)

print(integrity)

sock.close()

