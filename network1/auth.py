def calc_auth(base, secret, prime):
    return (base**secret) % prime


def dh_server(conn):
    shared_prime, shared_base = 23, 5

    server_secret = 16

    b = calc_auth(shared_base, server_secret, shared_prime)

    data = conn.recv(1024)
    a = int(data.decode('utf-8'))

    conn.send(bytes(str(b), 'ascii'))
    shared_open = calc_auth(a, server_secret, shared_prime)

    print("Shared key: " + str(shared_open))
    return shared_open


def dh_client(sock):
    shared_prime, shared_base = 23, 5

    client_secret = 19

    a = calc_auth(shared_base, client_secret, shared_prime)

    sock.send(bytes(str(a), 'ascii'))

    data = sock.recv(1024)
    b = int(data.decode('utf-8'))

    shared_open = calc_auth(b, client_secret, shared_prime)

    print("Shared key: " + str(shared_open))
    return shared_open


