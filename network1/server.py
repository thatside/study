import socket
import auth, common


sock = socket.socket()

sock.bind(("", 9009))
sock.listen(1)

conn, addr = sock.accept()

print('connected:' + addr[0])

auth.dh_server(conn)

common.receive_block(conn)

conn.close()

