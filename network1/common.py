import base64
from Crypto.Cipher import AES, XOR
from Crypto import Random

method = 'aes'

key = 'some private key'

iv = bytes(b'0000000000000000')

def prepare(message):
    message = bytearray(bytes(message, encoding='utf-8'))
    for i in range(len(message), 1023):
        message += bytes('0', encoding='utf-8')

    lrc = _lrc(message)
    message.append(lrc)

    return message


def send_block(sock, message):
    message = encode(message)
    print(message)
    sock.send(bytes(message))
    if receive_response(sock):
        return True
    else:
        return False


def receive_block(sock):
    message = bytearray(sock.recv(1024))
    message = bytearray(decode(message))
    print("Message: " + str(message))
    if check(message):
        sock.send(bytearray([bytes(255) in range(1024)]))
    else:
        sock.send(bytearray([bytes('0', encoding='utf-8') in range(1024)]))
    pass


def receive_response(sock):
    data = sock.recv(1024)
    if data == bytearray([bytes('0', encoding='utf-8') in range(1024)]):
        return True
    elif data == bytearray([bytes(255) in range(1024)]):
        return False

def encode(message):
    if method == 'base64':
        return base64.standard_b64encode(message)
    elif method == 'aes':
        return AES.new(key, AES.MODE_CBC, iv).encrypt(bytes(message))
    elif method == 'xor':
        return XOR.new(key).encrypt(bytes(message))


def decode(message):
    if method == 'base64':
        return base64.standard_b64decode(message)
    elif method == 'aes':
        return AES.new(key, AES.MODE_CBC, iv).decrypt(bytes(message))
    elif method == 'xor':
        return XOR.new(key).decrypt(bytes(message))


def check(message):
    present_lrc = message.pop()
    calc_lrc = _lrc(message)
    return present_lrc == calc_lrc


def _lrc(message):
    """Calc LRC of bytearray message"""
    lrc = 0
    for b in message:
        lrc ^= b
    return lrc