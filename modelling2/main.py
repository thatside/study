
import pylab
from matplotlib.mlab import frange


def euler(f, xa, xb, ya, h):
    n = int((xb - xa) / h + 1)
    x = x_const
    y = ya
    ylist = []
    xlist = []
    for i in range(n):
        y += h * f(x, y)
        ylist.append(y)
        x += h
        xlist.append(x)
    return ylist, xlist


x1s = 3000

ms = 2500
mk = 3300
dm = 400

cs = 20000
ck = 40000
dc = 10000

ks = 400
kk = 1100
dk = 350

t0 = 0
tk = 5
dt = 0.0005

x_const = 6


pylab.figure(1)
klist, mlist, clist = frange(ks, kk, dk).tolist(), frange(ms, mk, dm).tolist(), frange(cs, ck, dc).tolist()


def calc(k, m, c):
    p = m * 9.81
    ylist, xlist = euler(lambda x, y: ((p - k * y - c * x) / m), t0, tk, x1s, dt)

    # print(ylist, xlist)
    pylab.plot(xlist, ylist)

for k, m, c in zip(klist, mlist, clist):
    calc(k, m, c)

pylab.show()

