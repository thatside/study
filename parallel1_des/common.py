def increment(key, index):
    if index <= 7:
        if key[index] != 255:
            key[index] += 1
            return key
        else:
            key[index] = 0
            return increment(key, index+1)
    else:
        return False
