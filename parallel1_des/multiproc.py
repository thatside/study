from common import increment
import pyDes
from timeit import default_timer as timer

from multiprocessing import Process, Manager, Queue, Value


def decrypt(q, key, enc_data, finished):
    des = pyDes.des(key, pyDes.CBC, '\0'*8, padmode=pyDes.PAD_PKCS5)
    while True:
        possible_data = des.decrypt(enc_data)
        if not finished.value:
            if possible_data == b'test':
                q.put([key, possible_data])
                finished.value = True
                break
            else:
                key = increment(key, 0)
                des.setKey(key)



if __name__ == '__main__':
    enc_key = bytearray(b'\x00\x02\x00\x00\x00\x00\x00\x00')

    des = pyDes.des(enc_key, pyDes.CBC, '\0'*8, padmode=pyDes.PAD_PKCS5)

    data = des.encrypt('test')
    print(data)

    key = bytearray(b'\x00\x00\x00\x00\x00\x00\x00\x00')

    start = timer()
    result_queue = Queue()
    finished = Value('b', False)
    p1, p2, p3, p4 = Process(target=decrypt, args=(result_queue, bytearray(b'\x00\x00\x00\x00\x00\x00\x00\x00'), data, finished)),\
                     Process(target=decrypt, args=(result_queue, bytearray(b'\x44\x44\x44\x44\x44\x44\x44\x44'), data, finished)),\
                     Process(target=decrypt, args=(result_queue, bytearray(b'\x88\x88\x88\x88\x88\x88\x88\x88'), data, finished)),\
                     Process(target=decrypt, args=(result_queue, bytearray(b'\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc'), data, finished))

    p1.start()
    p2.start()
    p3.start()
    p4.start()
    while True:
        result = result_queue.get()
        print(result)
        break
    p1.terminate()
    p2.terminate()
    p3.terminate()
    p4.terminate()

    end = timer()
    print(end - start)
    print(key)

