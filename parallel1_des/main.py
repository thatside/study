from common import increment
import pyDes
from timeit import default_timer as timer


enc_key = bytearray(b'\x00\x02\x00\x00\x00\x00\x00\x00')

des = pyDes.des(enc_key, pyDes.CBC, '\0'*8, padmode=pyDes.PAD_PKCS5)

data = des.encrypt('test')
print(data)

key = bytearray(b'\x00\x00\x00\x00\x00\x00\x00\x00')

start = timer()
des2 = pyDes.des(key, pyDes.CBC, '\0'*8, padmode=pyDes.PAD_PKCS5)
while True:
    enc_data = des2.decrypt(data)
    if enc_data == b'test':
        break
    else:
        key = increment(key, 0)
        des2.setKey(key)

end = timer()
print(end - start)
print(key)

