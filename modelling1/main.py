import math
import matplotlib as mpl
import numpy as np
import matplotlib.pyplot as plt
import pylab
from matplotlib.mlab import frange

a = 0.8
c1 = 0.1
c2 = 3

t0 = 0
tk = 5
dt = 0.01

f0 = 5
fk = 60
df = 15


def func(t, f):
    p1 = math.exp(-a * t)
    p2 = c1 * math.cos(omega(f) * t)
    p3 = c2 * math.sin(omega(f) * t)
    return p1 * (p2 + p3)


def omega(x):
    return 2 * math.pi * x


xlist = frange(t0, tk, dt).tolist()
# flist = [20]
flist = frange(f0, fk, df, closed=0).tolist()
ylist = []

for f_step in flist:
    ylist.append([[func(x, f_step) for x in xlist], f_step])

pylab.figure(1)
for y in ylist:
    pylab.plot(xlist, y[0], label='f=' + str(y[1]))
    pylab.legend()

pylab.show()


