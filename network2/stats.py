import csv
import datetime
from matplotlib import pyplot as plt
import matplotlib.dates as mdates

years = mdates.YearLocator()   # every year
months = mdates.MonthLocator()  # every month
yearsFmt = mdates.DateFormatter('%Y')

def read_stats():
    csv_file = open('stats.csv', 'r')
    header = ['start_time', 'end_time', 'duration', 'size', 'speed']
    reader = csv.DictReader(csv_file, header)
    speeds = []
    dates = []
    next(reader, None)
    for row in reader:
        speeds.append(float(row['speed']))
        dates.append(row['end_time'])

    fig, ax = plt.subplots()

    ax.plot(dates, speeds)
    ax.xaxis.set_major_locator(years)
    ax.xaxis.set_major_formatter(yearsFmt)
    ax.xaxis.set_minor_locator(months)


    ax.show()


read_stats()