import socket
import csv
import datetime
from timeit import default_timer as timer

s = socket.socket()
s.bind(("localhost", 9999))
s.listen(10)

csv_file = open('stats.csv', 'w')
header = ['start_time', 'end_time', 'duration', 'size', 'speed']
writer = csv.DictWriter(csv_file, fieldnames=header)
writer.writeheader()

while True:
    sc, address = s.accept()

    print(address)

    f = open('temp.iso', 'wb')
    size = 0  # in KBs
    print('Transmission started')
    start = timer()
    start_time = datetime.datetime.now()
    while True:
        l = sc.recv(1024)
        size += 1
        while l:
            f.write(l)
            l = sc.recv(1024)
            size += 1
        else:
            break

    end = timer()
    end_time = datetime.datetime.now()
    print('Transmission ended')
    f.close()
    sc.close()

    duration = end - start
    speed = size / duration
    writer.writerow({'start_time': start_time, 'end_time': end_time, 'duration': duration, 'size': size, 'speed': speed})

s.close()
