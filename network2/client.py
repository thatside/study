import socket
import os
import csv
from timeit import default_timer as timer

filename = '/home/thatside/Downloads/ubuntu1404464.iso'

sock = socket.socket()

sock.connect(('localhost', 9999))

size = os.path.getsize(filename)
print('File size is {0} Kb'.format(size / 1024))

start = timer()
f = open(filename, "rb")
l = f.read(1024)
while l:
    sock.send(l)
    l = f.read(1024)

end = timer()

sock.close()
time = end - start
print('Transmission lasted {0} seconds'.format(time))
print('Transmission speed was {0} Kb/s'.format(size / time / 1024))
