import matplotlib.pyplot as plt
import numpy
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter


def euler(t_a, t_b, dt, x_0, x1_0, y_0, y1_0, k_fr, c, m, g):
    """
    t_a, t_b, dt - начальное, конечное, шаг времени
    x_0, x1_0 - начальное значение положения и скорости клети
    y_0, y1_0 - начальное значение положения и скорости груза
    k_fr - коэффициент трения
    c - коэффициент упругости
    m - масса груза
    g - ускорение свободного падения
    """
    n = (t_b - t_a) / dt
    t = t_a
    x = x_0
    y = y_0
    x1 = x1_0
    y1 = y1_0

    xlist, x1list = [], []
    ylist, y1list = [], []

    for i in range(int(n)):
        x1 += (g - (k_fr * x1 + c * (y - x)) / m) * dt
        x1list.append(x1)

        y1 += (g - (c * (y - x) / m)) * dt
        y1list.append(y1)

        x += x1 * dt
        xlist.append(x)

        y += y1 * dt
        ylist.append(y)

    return numpy.array(xlist), numpy.array(ylist)


x0 = 0.05
y0 = 0
x10 = 10
y10 = 10
m = 7000
c = 40000
k = 6000
t_0 = 0
t_k = 2
dt = 0.0005
g = 9.81

plt.figure(1)

X, Y = euler(t_0, t_k, dt, x0, x10, y0, y10, k, c, m, g)
Z = numpy.arange(t_0, t_k, dt)

plt.plot(Z, X)
plt.figure(2)
plt.plot(Z, Y)

plt.show()
